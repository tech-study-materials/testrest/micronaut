package com.example.todoer;

import io.micronaut.http.annotation.*;
import io.micronaut.runtime.Micronaut;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

import static java.util.stream.Collectors.toUnmodifiableList;

@Controller
public class TodoerApplication implements TodoerApi {

    public static void main(String[] args) {
        Micronaut.run(TodoerApplication.class, args);
    }

    @Inject Repo repo;

    @Override
    public IdResponse createTodo(String user, NewTodoRequest req) {
        return new IdResponse(repo.create(user, req.getDescription()));
    }

    @Override
    public List<Todo> getTodos(String user) {
        return repo.findByUser(user);
    }

    @Override
    public void markDone(String user, long id) {
        repo.deleteById(id);
    }

}

interface TodoerApi {
    @Post("/{user}/todo")
    IdResponse createTodo(@PathVariable String user, @Body NewTodoRequest req);

    @Get("/{user}/todo")
    List<Todo> getTodos(@PathVariable String user);

    @Delete("/{user}/todo/{id}")
    void markDone(@PathVariable String user, @PathVariable long id);
}

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
class Todo {
    long id;
    String user;
    String description;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class NewTodoRequest {
    String description;
}

@Data
@AllArgsConstructor
@NoArgsConstructor
class IdResponse {
    long id;
}

interface Repo {
    long create(String user, String description);
    List<Todo> findByUser(String user);
    void deleteById(long id);
    void deleteAll();
}

@Singleton
class InMemRepo implements Repo {
    final AtomicLong sequence = new AtomicLong();
    List<Todo> todos = new ArrayList<>();

    @Override
    public long create(String user, String description) {
        long id = sequence.incrementAndGet();
        todos.add(new Todo(id, user, description));
        return id;
    }

    @Override
    public List<Todo> findByUser(String user) {
        return todos.stream()
                .filter(t -> user.equals(t.user))
                .collect(toUnmodifiableList());
    }

    @Override
    public void deleteById(long id) {
        todos.stream()
                .filter(t -> id == t.id)
                .findFirst()
                .ifPresent(t -> todos.remove(t));
    }

    @Override
    public void deleteAll() {
        todos.clear();
    }
}