package com.example.todoer;

import io.micronaut.context.annotation.Primary;
import io.micronaut.http.client.annotation.Client;
import io.micronaut.test.extensions.junit5.annotation.MicronautTest;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;

import static org.assertj.core.api.Assertions.assertThat;

@Client("/") @Primary
interface TodoerClient extends TodoerApi {}

@MicronautTest
public class TodoerApplicationTest {

    @Inject TodoerApi api;
    @Inject Repo repo;

    @BeforeEach
    void setup() {
        System.out.println("api is : " + api.getClass().getSimpleName());
        repo.deleteAll();
    }

    @Test
    void testCreateAndGet() {
        //given
        api.createTodo("adam", new NewTodoRequest("buy milk"));

        //when
        var todos = api.getTodos("adam");

        //then
        assertThat(todos)
                .usingElementComparatorIgnoringFields("id")
                .containsExactlyInAnyOrder(
                        Todo.builder().user("adam").description("buy milk").build()
                );
    }

    @Test
    void testMarkDone() {
        //given
        long buyMilkId = api
                .createTodo("adam", new NewTodoRequest("buy milk"))
                .getId();

        //when
        api.markDone("adam", buyMilkId);
        var todos = api.getTodos("adam");

        //then
        assertThat(todos).isEmpty();
    }
}
